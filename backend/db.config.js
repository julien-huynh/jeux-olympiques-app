/* Import des modules nécessaires */
const { Sequelize } = require("sequelize");

/* Connexion à la base de données */
let sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USER,
  process.env.DB_PASS,
  {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: "postgres",
    logging: false,
  }
);

/* Relations SQL entre les tables */

const db = {};
db.sequelize = sequelize;
db.User = require("./models/user")(sequelize);
db.Medal = require("./models/medal")(sequelize);
db.Country = require("./models/country")(sequelize);
db.Sport = require("./models/sport")(sequelize);
db.Athlete = require("./models/athlete")(sequelize);
db.Host = require("./models/host")(sequelize);
db.Event = require("./models/event")(sequelize);
db.EventType = require("./models/eventType")(sequelize);
db.Category = require("./models/category")(sequelize);
db.Result = require("./models/result")(sequelize);

/* ManyToOne Athlete Country */
db.Country.hasMany(db.Athlete);
db.Athlete.belongsTo(db.Country);

/* ManyToOne Medal Result */
db.Medal.hasMany(db.Result);
db.Result.belongsTo(db.Medal);

/* ManyToOne Event Sport */
db.Sport.hasMany(db.Event);
db.Event.belongsTo(db.Sport);

/* ManyToOne Category Event */
db.Category.hasMany(db.Event);
db.Event.belongsTo(db.Category);

/* ManyToOne Athlete Result */
db.Athlete.hasMany(db.Result);
db.Result.belongsTo(db.Athlete);

/* ManyToOne Event Result */
db.Event.hasMany(db.Result);
db.Result.belongsTo(db.Event);

/* ManyToOne EventType Event */
db.EventType.hasMany(db.Event);
db.Event.belongsTo(db.EventType);

/* ManyToOne Host Event */
db.Host.hasMany(db.Event);
db.Event.belongsTo(db.Host);

/* Synchronisation des modèles */
// Pour reset la bdd en cas de modification du code : mettre {alter:true} ou {force:true} ou les deux dans sync() comme ceci sync({alter:true},{force:true}). N'oubliez pas de l'enlever après !!
sequelize.sync();

module.exports = db;
