/* Import des modules nécéssaires */
const DB = require("../db.config");
const { Op } = require("sequelize");
const Event = DB.Event;
const Result = DB.Result;
const Host = DB.Host;
const Sport = DB.Sport;
const EventType = DB.EventType;
const Category = DB.Category;
const { RequestError, EventError } = require("../error/customError");
const { addAssociation } = require("../_utils/associationUtils");

/* Récupération de tous les évènements, leurs types d'évènements, leurs hotes, leurs catégorie et leurs sport */
exports.getAllEvents = (req, res, next) => {
  // Récupération des évènements
  Event.findAll()
    .then((events) => res.json({ data: events }))
    .catch((err) => next(err));
};

/* Récupération d'un évènement, son type d'évènement, ses hotes, sa catégorie, son sport et ses résultats en fonction de l'ID*/
exports.getEventById = async (req, res, next) => {
  try {
    // Récupération du paramètre
    let eventId = parseInt(req.params.id);

    // Vérification de l'existance du paramètre
    if (!eventId) {
      throw new RequestError("Id manquant.");
    }

    // Récupération de l'évènement
    let event = await Event.findOne({
      where: { id: eventId },
      include: [
        { model: Host },
        { model: Sport },
        { model: Category },
        { model: EventType },
        { model: Result },
      ],
    });

    // Vérification de l'existance de l'évènement
    if (event === null) {
      throw new EventError("Cet évènement n'existe pas .", 0);
    }

    // Réponse de la requète
    return res.json({ data: event });
  } catch (err) {
    next(err);
  }
};

/* Récupération d'un évènement, son type d'évènement, ses hotes, sa catégorie, son sport et ses résultats en fonction de leurs ID*/
exports.getEventByParams = async (req, res, next) => {
  try {
    // Récupération des paramètres
    let { sportId, eventTypeId, categoryId, hostId } = req.query;

    // Vérification de l'existance des paramètres
    if (!sportId || !eventTypeId || !categoryId || !hostId) {
      throw new RequestError("Paramètre(s) manquant(s).");
    }

    // Récupération de l'évènement
    let event = await Event.findOne({
      where: {
        SportId: parseInt(sportId),
        CategoryId: parseInt(categoryId),
        EventTypeId: parseInt(eventTypeId),
        HostId: parseInt(hostId),
      },
      include: [
        { model: Host },
        { model: Sport },
        { model: Category },
        { model: EventType },
        { model: Result },
      ],
    });

    // Vérification de l'existence de l'évènement
    if (event === null) {
      throw new EventError("Cet évènement n'existe pas.", 0);
    }

    // Réponse de la requête
    return res.json({ data: event });
  } catch (err) {
    next(err);
  }
};

/* Ajout d'un évènement */
exports.addEvent = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    const { date, eventTypeId, categoryId, sportId, hostId } = req.body;

    const whereClause = {};

    // Récupération des paramètres entrés
    if (eventTypeId) {
      whereClause.EventTypeId = eventTypeId;
    }
    if (categoryId) {
      whereClause.CategoryId = categoryId;
    }
    if (sportId) {
      whereClause.SportId = sportId;
    }
    if (hostId) {
      whereClause.HostId = hostId;
    }

    // Recherche de l'évènement en fonction des critères
    let event = await Event.findOne({
      where: whereClause,
      raw: true,
    });

    // Vérification de l'existance d'un évènement avec les memes paramètres
    if (event !== null) {
      throw new RequestError(
        `Un évènement avec ces paramètres existe déja dans la base de données.`,
        1
      );
    }

    // Création de l'évènement dans la base de données
    let eventc = await Event.create(req.body, {
      include: [
        { model: Category },
        { model: EventType },
        { model: Sport },
        { model: Host },
      ],
    });

    const eventTypeResult = await addAssociation(
      eventTypeId,
      EventType,
      "addEvent",
      "Le type d'évènements spécifié n'existe pas.",
      eventc
    );
    const categoryResult = await addAssociation(
      categoryId,
      Category,
      "addEvent",
      "La catégorie spécifiée n'existe pas.",
      eventc
    );
    const sportResult = await addAssociation(
      sportId,
      Sport,
      "addEvent",
      "Le Sport spécifié n'existe pas.",
      eventc
    );
    const hostResult = await addAssociation(
      hostId,
      Host,
      "addEvent",
      "L'hôte spécifié n'existe pas.",
      eventc
    );

    // Preparation du message de réponse
    let message = "L'évènement a bien été créé.";
    if (!eventTypeResult.success) message += " " + eventTypeResult.message;
    if (!categoryResult.success) message += " " + categoryResult.message;
    if (!sportResult.success) message += " " + sportResult.message;
    if (!hostResult.success) message += " " + hostResult.message;

    // Envoi de la réponse de la requète
    return res.json({
      message,
    });
  } catch (err) {
    next(err);
  }
};

/* Modification d'un évènement, avec son type d'évènements, et son sport */
exports.updateEvent = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    let eventId = parseInt(req.params.id);
    const { date, eventTypeId, categoryId, sportId, hostId } = req.body;

    // Vérification de l'existance du paramètre
    if (!eventId) {
      throw new RequestError("Id de l'évènement manquant.");
    }

    // Récupération de l'évènement
    let event = await Event.findOne({
      where: { id: eventId },
      raw: true,
    });

    // Vérification de l'existance de l'évènement
    if (event === null) {
      throw new EventError("Cet évènement n'existe pas .", 0);
    }

    const whereClause = {
      EventTypeId: event.EventTypeId,
      CategoryId: event.CategoryId,
      SportId: event.SportId,
      HostId: event.HostId,
      id: { [Op.ne]: eventId },
    };

    // Récupération des paramètres entrés
    if (eventTypeId) {
      whereClause.EventTypeId = eventTypeId;
    }
    if (categoryId) {
      whereClause.CategoryId = categoryId;
    }
    if (sportId) {
      whereClause.SportId = sportId;
    }
    if (hostId) {
      whereClause.HostId = hostId;
    }

    // Recherche de l'évènement en fonction des critères
    event = await Event.findOne({
      where: whereClause,
      raw: true,
    });

    // Vérification de l'existance d'un évènement avec les memes paramètres
    if (event !== null) {
      throw new EventError(
        `Un autre évènement avec ces paramètres existe déja dans la base de données.`,
        1
      );
    }

    // Modification de l'évènement dans la base de données
    let eventu = await Event.update(req.body, { where: { id: eventId } });

    const eventTypeResult = await addAssociation(
      eventTypeId,
      EventType,
      "addEvent",
      "Le type d'évènements spécifié n'existe pas.",
      eventu
    );
    const categoryResult = await addAssociation(
      categoryId,
      Category,
      "addEvent",
      "La catégorie spécifiée n'existe pas.",
      eventu
    );
    const sportResult = await addAssociation(
      sportId,
      Sport,
      "addEvent",
      "Le Sport spécifié n'existe pas.",
      eventu
    );
    const hostResult = await addAssociation(
      hostId,
      Host,
      "addEvent",
      "L'hôte spécifié n'existe pas.",
      eventu
    );

    // Prepare the response message
    let message = "L'évènement a bien été modifié.";
    if (!eventTypeResult.success) message += " " + eventTypeResult.message;
    if (!categoryResult.success) message += " " + categoryResult.message;
    if (!sportResult.success) message += " " + sportResult.message;
    if (!hostResult.success) message += " " + hostResult.message;

    // Envoi de la réponse de la requète
    return res.json({
      message,
    });
  } catch (err) {
    next(err);
  }
};

/* Suppression d'un évènement */
exports.deleteEvent = async (req, res, next) => {
  try {
    // Récupération du paramètre de la requète
    let eventId = parseInt(req.params.id);

    // Vérification de l'existance du paramètre
    if (!eventId) {
      throw new RequestError("Id de l'évènement manquant.");
    }

    // Suppression de l'évènement dans la base de données
    await Event.destroy({ where: { id: eventId }, force: true });

    // Envoi de la réponse de la requète
    return res.status(204).json();
  } catch (err) {
    next(err);
  }
};
