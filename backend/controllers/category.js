/* Import des modules nécéssaires */
const DB = require("../db.config");
const { Op } = require("sequelize");
const Category = DB.Category;
const Event = DB.Event;

const { RequestError, CategoryError } = require("../error/customError");

/* Récupération de tous les categorys */
exports.getAllCategories = (req, res, next) => {
  // Récupération des categories
  Category.findAll()
    .then((categories) => res.json({ data: categories }))
    .catch((err) => next(err));
};

/* Récupération d'une categorie en fonction de l'Id */
exports.getCategoryById = async (req, res, next) => {
  try {
    // Récupération du paramètre
    let categoryId = parseInt(req.params.id);

    // Vérification de l'existance du paramètre
    if (!categoryId) {
      throw new RequestError("Paramètre(s) manquant(s) .");
    }

    // Récupération de la categorie
    let category = await Category.findOne({
      where: { id: categoryId },
      include: [{ model: Event }],
    });

    // Vérification de l'existance de la categorie
    if (category === null) {
      throw new CategoryError("Cette categorie n'existe pas .", 1);
    }

    // Réponse de la requète
    return res.json({ data: category });
  } catch (err) {
    next(err);
  }
};

/* Récupération d'une categorie en fonction du nom */
exports.getCategoryByName = async (req, res, next) => {
  try {
    // Récupération du paramètre
    let categoryName = req.params.name;

    // Vérification de l'existance du paramètre
    if (!categoryName) {
      throw new RequestError("Paramètre(s) manquant(s) .");
    }

    // Récupération de la categorie
    let category = await Category.findOne({
      where: { name: categoryName },
      include: [{ model: Event }],
    });

    // Vérification de l'existance du category
    if (category === null) {
      throw new CategoryError("Cette categorie n'existe pas .", 0);
    }

    // Réponse de la requète
    return res.json({ data: category });
  } catch (err) {
    next(err);
  }
};

/* Ajout d'un category */
exports.addCategory = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    const { name } = req.body;

    // Vérification de l'existance des paramètres
    if (!name) {
      throw new RequestError("Paramètre(s) manquant(s) .");
    }

    // Récupération du category du même nom
    let category = await Category.findOne({ where: { name: name }, raw: true });

    // Vérification de l'existance du category du même nom
    if (category !== null) {
      throw new RequestError(
        `La Catégorie ${name} existe déja dans la base de données.`,
        1
      );
    }

    // Création du category dans la base de données
    let categoryc = await Category.create(req.body);

    // Envoi de la réponse de la requète
    return res.json({
      message: "La categorie à bien été créé .",
      data: categoryc,
    });
  } catch (err) {
    next(err);
  }
};

/* Modification d'une categorie */
exports.updateCategory = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    let categoryId = parseInt(req.params.id);
    const { name } = req.body;

    // Vérification de l'existance du paramètre
    if (!categoryId) {
      throw new RequestError("Id de la categorie manquant.");
    }

    // Récupération de la categorie
    let category = await Category.findOne({
      where: { id: categoryId },
      raw: true,
    });

    // Vérification de l'existance de la categorie
    if (category === null) {
      throw new CategoryError("Cette categorie n'existe pas .", 0);
    }

    // Si un nom a été donné en paramètres
    if (name) {
      // Vérification de l'existance de la categorie avec le nom donné en paramètre
      category = await Category.findOne({
        where: { name: name, id: { [Op.ne]: categoryId } },
        raw: true,
      });
      if (category !== null) {
        throw new RequestError(
          `La categorie ${name} existe déja dans la base de données.`,
          1
        );
      }
    }

    // Modification de la categorie dans la base de données
    await Category.update(req.body, { where: { id: categoryId } });

    // Envoi de la réponse de la requète
    return res.json({
      message: "La categorie à bien été modifié .",
    });
  } catch (err) {
    next(err);
  }
};

// exports.untrashCategory = async (req, res, next) => {
// 	try {
// 		let categoryId = parseInt(req.params.id);

// 		if (!categoryId) {
// 			throw new RequestError("Paramètre(s) manquant(s) .");
// 		}

// 		await Category.restore({ where: { id: categoryId } });

// 		return res.status(204).json({});
// 	} catch (err) {
// 		next(err);
// 	}
// };

// exports.trashCategory = async (req, res, next) => {
// 	try {
// 		let categoryId = parseInt(req.params.id);

// 		if (!categoryId) {
// 			throw new RequestError("Paramètre(s) manquant(s) .");
// 		}

// 		await Category.destroy({ where: { id: categoryId } });

// 		return res.status(204).json({});
// 	} catch (err) {
// 		next(err);
// 	}
// };

/* Suppression d'une categorie */
exports.deleteCategory = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    let categoryId = parseInt(req.params.id);

    // Vérification de l'existance du paramètre
    if (!categoryId) {
      throw new RequestError("Id de la catégorie manquant.");
    }

    // Suppression de la categorie dans la base de données
    await Category.destroy({ where: { id: categoryId }, force: true });

    // Envoi de la réponse de la requète
    return res.status(204).json({});
  } catch (err) {
    next(err);
  }
};
