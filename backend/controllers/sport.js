/* Import des modules nécéssaires */
const DB = require("../db.config");
const slugify = require("slugify");
const { Op } = require("sequelize");
const Sport = DB.Sport;
const Event = DB.Event;

const { RequestError, SportError } = require("../error/customError");

/* Récupération de tous les sports */
exports.getAllSports = (req, res, next) => {
  // Récupération des sports
  Sport.findAll()
    .then((sports) => res.json({ data: sports }))
    .catch((err) => next(err));
};

/* Récupération d'un sport en fonction de l'Id */
exports.getSportById = async (req, res, next) => {
  try {
    // Récupération du paramètre
    let sportId = parseInt(req.params.id);

    // Vérification de l'existance du paramètre
    if (!sportId) {
      throw new RequestError("Paramètre(s) manquant(s) .");
    }

    // Récupération du sport
    let sport = await Sport.findOne({
      where: { id: sportId },
      include: [{ model: Event }],
    });

    // Vérification de l'existance du sport
    if (sport === null) {
      throw new SportError("Ce sport n'existe pas .", 0);
    }

    // Réponse de la requète
    return res.json({ data: sport });
  } catch (err) {
    next(err);
  }
};

/* Récupération d'un sport en fonction du nom */
exports.getSportByName = async (req, res, next) => {
  try {
    // Récupération du paramètre
    let sportName = req.params.name;

    // Vérification de l'existance du paramètre
    if (!sportName) {
      throw new RequestError("Paramètre(s) manquant(s) .");
    }

    // Récupération du sport
    let sport = await Sport.findOne({
      where: { name: sportName },
      include: [{ model: Event }],
    });

    // Vérification de l'existance du sport
    if (sport === null) {
      throw new SportError("Ce sport n'existe pas .", 0);
    }

    // Réponse de la requète
    return res.json({ data: sport });
  } catch (err) {
    next(err);
  }
};

/* Ajout d'un sport */
exports.addSport = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    const { name, image } = req.body;

    // Vérification de l'existance des paramètres
    if (!name) {
      throw new RequestError("Paramètre(s) manquant(s) .");
    }

    // Récupération du sport du même nom
    let sport = await Sport.findOne({ where: { name: name }, raw: true });

    // Vérification de l'existance du sport du même nom
    if (sport !== null) {
      throw new RequestError(
        `Le Sport ${name} existe déja dans la base de données.`,
        1
      );
    }

    // Génération du slug avec le nom du sport
    req.body.slug = slugify(name);

    // Création du sport dans la base de données
    let sportc = await Sport.create(req.body);

    // Envoi de la réponse de la requète
    return res.json({
      message: "Le sport à bien été créé .",
      data: sportc,
    });
  } catch (err) {
    next(err);
  }
};

/* Modification d'un sport */
exports.updateSport = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    let sportId = parseInt(req.params.id);
    const { name, image } = req.body;

    // Vérification de l'existance du paramètre
    if (!sportId) {
      throw new RequestError("Id du sport manquant.");
    }

    // Récupération du sport
    let sport = await Sport.findOne({ where: { id: sportId }, raw: true });

    // Vérification de l'existance du sport
    if (sport === null) {
      throw new SportError("Ce sport n'existe pas .", 0);
    }

    // Si un nom a été donné en paramètres
    if (name) {
      // Vérification de l'existance du sport avec le nom donné en paramètre
      sport = await Sport.findOne({
        where: { name: name, id: { [Op.ne]: sportId } },
        raw: true,
      });
      if (sport !== null) {
        throw new RequestError(
          `Le sport ${name} existe déja dans la base de données.`,
          1
        );
      }
      // Génération du slug avec le nom du sport
      req.body.slug = slugify(name);
    }

    // Modification du sport dans la base de données
    await Sport.update(req.body, { where: { id: sportId } });

    // Envoi de la réponse de la requète
    return res.json({
      message: "Le sport à bien été modifié .",
    });
  } catch (err) {
    next(err);
  }
};

// exports.untrashSport = async (req, res, next) => {
// 	try {
// 		let sportId = parseInt(req.params.id);

// 		if (!sportId) {
// 			throw new RequestError("Paramètre(s) manquant(s) .");
// 		}

// 		await Sport.restore({ where: { id: sportId } });

// 		return res.status(204).json({});
// 	} catch (err) {
// 		next(err);
// 	}
// };

// exports.trashSport = async (req, res, next) => {
// 	try {
// 		let sportId = parseInt(req.params.id);

// 		if (!sportId) {
// 			throw new RequestError("Paramètre(s) manquant(s) .");
// 		}

// 		await Sport.destroy({ where: { id: sportId } });

// 		return res.status(204).json({});
// 	} catch (err) {
// 		next(err);
// 	}
// };

/* Suppression d'un sport */
exports.deleteSport = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    let sportId = parseInt(req.params.id);

    // Vérification de l'existance du paramètre
    if (!sportId) {
      throw new RequestError("Id du sport manquant.");
    }

    // Suppression du sport dans la base de données
    await Sport.destroy({ where: { id: sportId }, force: true });

    // Envoi de la réponse de la requète
    return res.status(204).json({});
  } catch (err) {
    next(err);
  }
};
