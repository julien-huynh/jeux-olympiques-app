/* Import des modules nécéssaires */
const DB = require("../db.config");
const slugify = require("slugify");
const { Op } = require("sequelize");
const Country = DB.Country;
const Athlete = DB.Athlete;

const { RequestError, CountryError } = require("../error/customError");

/* Récupération de tous les pays */
exports.getAllCountries = (req, res, next) => {
  // Récupération des pays
  Country.findAll()
    .then((countries) => res.json({ data: countries }))
    .catch((err) => next(err));
};

/* Récupération d'un pays en fonction de l'Id */
exports.getCountryById = async (req, res, next) => {
  try {
    // Récupération du paramètre
    let countryId = parseInt(req.params.id);

    // Vérification de l'existance du paramètre
    if (!countryId) {
      throw new RequestError("Paramètre(s) manquant(s) .");
    }

    // Récupération du pays
    let country = await Country.findOne({
      where: { id: countryId },
      include: [{ model: Athlete }],
    });

    // Vérification de l'existance du pays
    if (country === null) {
      throw new CountryError("Cet country n'existe pas .", 0);
    }

    // Réponse de la requète
    return res.json({ data: country });
  } catch (err) {
    next(err);
  }
};

/* Récupération d'un pays en fonction du nom */
exports.getCountryByName = async (req, res, next) => {
  try {
    // Récupération du paramètre
    let countryName = req.params.name;

    // Vérification de l'existance du paramètre
    if (!countryName) {
      throw new RequestError("Paramètre(s) manquant(s) .");
    }

    // Récupération du pays
    let country = await Country.findOne({
      where: { name: countryName },
      include: [{ model: Athlete }],
    });

    // Vérification de l'existance du pays
    if (country === null) {
      throw new CountryError("Cet country n'existe pas .", 0);
    }

    // Réponse de la requète
    return res.json({ data: country });
  } catch (err) {
    next(err);
  }
};

/* Récupération d'un pays en fonction du nom */
exports.getCountryByCode = async (req, res, next) => {
  try {
    // Récupération du paramètre
    let countryCode = req.params.code;

    // Vérification de l'existance du paramètre
    if (!countryCode) {
      throw new RequestError("Paramètre(s) manquant(s) .");
    }

    // Récupération du pays
    let country = await Country.findOne({
      where: { code: countryCode },
      include: [{ model: Athlete }],
    });

    // Vérification de l'existance du pays
    if (country === null) {
      throw new CountryError("Cet country n'existe pas .", 0);
    }

    // Réponse de la requète
    return res.json({ data: country });
  } catch (err) {
    next(err);
  }
};

/* Ajout d'un pays */
exports.addCountry = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    const { name, flag, code, longcode } = req.body;

    // Vérification de l'existance des paramètres
    if (!name || !longcode) {
      throw new RequestError("Paramètre(s) manquant(s) .");
    }

    // Récupération du pays du même nom
    let country = await Country.findOne({ where: { name: name }, raw: true });

    // Vérification de l'existance du pays du même nom
    if (country !== null) {
      throw new RequestError(
        `Le pays ${name} existe déja dans la base de données.`,
        1
      );
    }

    // Récupération du pays du même code
    country = await Country.findOne({
      where: { longcode: longcode },
      raw: true,
    });

    // Vérification de l'existance du pays du même nom
    if (country !== null) {
      throw new RequestError(
        `Le pays avec le code ${longcode} existe déja dans la base de données.`,
        1
      );
    }

    // Génération du slug avec le nom du pays
    req.body.slug = slugify(name.toLowerCase());

    // Création du pays dans la base de données
    let countryc = await Country.create(req.body);

    // Envoi de la réponse de la requète
    return res.json({
      message: "Le pays à bien été créé .",
      data: countryc,
    });
  } catch (err) {
    next(err);
  }
};

/* Modification d'un pays */
exports.updateCountry = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    let countryId = parseInt(req.params.id);
    const { name, flag, code, longcode } = req.body;

    // Vérification de l'existance du paramètre
    if (!countryId) {
      throw new RequestError("Id du pays manquant.");
    }

    // Récupération du pays
    let country = await Country.findOne({
      where: { id: countryId },
      raw: true,
    });

    // Vérification de l'existance du pays
    if (country === null) {
      throw new CountryError("Ce pays n'existe pas .", 0);
    }

    // Vérifie si un nom a été donné en paramètre et traite en conséquence
    if (name) {
      await verifyCountryExists(
        { name: name, id: { [Op.ne]: countryId } },
        name
      );
      req.body.slug = slugify(name);
    }

    // Vérifie si un longcode a été donné en paramètre et traite en conséquence
    if (longcode) {
      await verifyCountryExists(
        { longcode: longcode, id: { [Op.ne]: countryId } },
        longcode
      );
    }

    async function verifyCountryExists(whereClause, paramName) {
      const country = await Country.findOne({ where: whereClause, raw: true });
      if (country !== null) {
        throw new RequestError(
          `Le paramètre ${paramName} existe déjà dans la base de données.`,
          1
        );
      }
    }

    // Modification du pays dans la base de données
    await Country.update(req.body, { where: { id: countryId } });

    // Envoi de la réponse de la requète
    return res.json({
      message: "Le pays à bien été modifié .",
    });
  } catch (err) {
    next(err);
  }
};

// exports.untrashCountry = async (req, res, next) => {
// 	try {
// 		let countryId = parseInt(req.params.id);

// 		if (!countryId) {
// 			throw new RequestError("Paramètre(s) manquant(s) .");
// 		}

// 		await Country.restore({ where: { id: countryId } });

// 		return res.status(204).json({});
// 	} catch (err) {
// 		next(err);
// 	}
// };

// exports.trashCountry = async (req, res, next) => {
// 	try {
// 		let countryId = parseInt(req.params.id);

// 		if (!countryId) {
// 			throw new RequestError("Paramètre(s) manquant(s) .");
// 		}

// 		await Country.destroy({ where: { id: countryId } });

// 		return res.status(204).json({});
// 	} catch (err) {
// 		next(err);
// 	}
// };

/* Suppression d'un pays */
exports.deleteCountry = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    let countryId = parseInt(req.params.id);

    // Vérification de l'existance du paramètre
    if (!countryId) {
      throw new RequestError("Id du pays manquant.");
    }

    // Suppression du pays dans la base de données
    await Country.destroy({ where: { id: countryId }, force: true });

    // Envoi de la réponse de la requète
    return res.status(204).json({});
  } catch (err) {
    next(err);
  }
};
