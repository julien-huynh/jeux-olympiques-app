/* Import des modules nécéssaires */
const DB = require("../db.config");
const { Op } = require("sequelize");
const Event = DB.Event;
const Result = DB.Result;
const Athlete = DB.Athlete;
const Sport = DB.Sport;
const Host = DB.Host;
const Medal = DB.Medal;
const EventType = DB.EventType;
const Category = DB.Category;
const {
  RequestError,
  ResultError,
  EventError,
} = require("../error/customError");
const { addAssociation } = require("../_utils/associationUtils");

/* Récupération de tous les Résultats, leur joueur, leur evenement et leur médaille */
exports.getAllResults = (req, res, next) => {
  // Récupération des résultats
  Result.findAll()
    .then((results) => res.json({ data: results }))
    .catch((err) => next(err));
};

/* Récupération d'un résultat, son joueur, sa médaille, son evenement en fonction de l'ID*/
exports.getResultById = async (req, res, next) => {
  try {
    // Récupération du paramètre
    let resultId = parseInt(req.params.id);

    // Vérification de l'existance du paramètre
    if (!resultId) {
      throw new RequestError("Id manquant.");
    }

    // Récupération du résultat
    let result = await Result.findOne({
      where: { id: resultId },
      include: [
        { model: Athlete },
        {
          model: Event,
          include: [
            { model: EventType },
            { model: Sport },
            { model: Category },
            { model: Host },
          ],
        },
        { model: Medal },
      ],
    });

    // Vérification de l'existance du résultat
    if (result === null) {
      throw new ResultError("Ce résultat n'existe pas .", 0);
    }

    // Réponse de la requète
    return res.json({ data: result });
  } catch (err) {
    next(err);
  }
};

/* Ajout d'un résultat */
exports.addResult = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    const { eventId, athleteId, medalId } = req.body;

    // Vérification de l'existance des paramètres
    if (!eventId) {
      throw new RequestError("Paramètre(s) manquant(s) .");
    }

    // Recherche de l'évènement en paramètres
    let event = await Event.findOne({
      where: { id: eventId },
      raw: true,
    });

    // Vérification de l'existance de l'évènement
    if (event === null) {
      throw new EventError("Cet évènement n'existe pas .", 0);
    }

    const whereClause = {
      EventId: eventId
    };

    // Récupération des paramètres entrés
    if (athleteId) {
      whereClause.AthleteId = athleteId;
    }
    if (medalId) {
      whereClause.MedalId = medalId;
    }
    // Recherche du résultat en fonction des critères
    let result = await Result.findOne({
      where: whereClause,
      raw: true,
    });

    // Vérification de l'existance d'un résultat avec les memes paramètres
    if (result !== null) {
      throw new RequestError(
        `Un résultat avec ces paramètres existe déja dans la base de données.`,
        1
      );
    }

    // Création du résultat dans la base de données
    let resultc = await Result.create(req.body, {
      include: [{ model: Athlete }, { model: Event }, { model: Medal }],
    });

    const medalResult = await addAssociation(
      medalId,
      Medal,
      "addResult",
      "La médaille spécifiée n'existe pas.",
      resultc
    );
    const athleteResult = await addAssociation(
      athleteId,
      Athlete,
      "addResult",
      "L'athlete spécifié n'existe pas.",
      resultc
    );

    const eventResult = await addAssociation(
        eventId,
        Event,
        "addResult",
        "L'évènement spécifié n'existe pas.",
        resultc
      );

    // Prepare the response message
    let message = "Le résultat a bien été créé.";
    if (!medalResult.success) message += " " + medalResult.message;
    if (!athleteResult.success) message += " " + athleteResult.message;
    if (!eventResult.success) message += " " + eventResult.message;

    // Envoi de la réponse de la requète
    return res.json({
      message,
    });
  } catch (err) {
    next(err);
  }
};

/* Modification d'un résultat, avec son type d'résultats, et son sport */
exports.updateResult = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    let resultId = parseInt(req.params.id);
    const { eventId, athleteId, medalId } = req.body;

    // Vérification de l'existance du paramètre
    if (!resultId) {
      throw new RequestError("Id de le résultat manquant.");
    }

    // Récupération de l'résultat
    let result = await Result.findOne({
      where: { id: resultId },
      raw: true,
    });

    // Vérification de l'existance de l'résultat
    if (result === null) {
      throw new ResultError("Ce résultat n'existe pas .", 0);
    }

    const whereClause = {
      EventId: result.EventId,
      AthleteId: result.AthleteId,
      MedalId: result.MedalId,
      id: { [Op.ne]: resultId },
    };

    // Récupération des paramètres entrés
    if (athleteId) {
      whereClause.AthleteId = athleteId;
    }
    if (medalId) {
      whereClause.MedalId = medalId;
    }
    if (eventId) {
      whereClause.EventId = eventId;
    }

    // Recherche de l'résultat en fonction des critères
    result = await Result.findOne({
      where: whereClause,
      raw: true,
    });

    // Vérification de l'existance d'un résultat avec les memes paramètres
    if (result !== null) {
      throw new ResultError(
        `Un autre résultat avec ces paramètres existe déja dans la base de données.`,
        1
      );
    }

    // Modification de l'résultat dans la base de données
    let resultu = await Result.update(req.body, { where: { id: resultId } });

    const athleteResult = await addAssociation(
      athleteId,
      Athlete,
      "addResult",
      "L'athlète spécifié n'existe pas.",
      resultu
    );
    const medalResult = await addAssociation(
      medalId,
      Medal,
      "addResult",
      "La médaille spécifiée n'existe pas.",
      resultu
    );
    const eventResult = await addAssociation(
      eventId,
      Event,
      "addResult",
      "L'évènement spécifié n'existe pas.",
      resultu
    );

    // Prepare the response message
    let message = "Le résultat a bien été modifié.";
    if (!athleteResult.success) message += " " + athleteResult.message;
    if (!medalResult.success) message += " " + medalResult.message;
    if (!eventResult.success) message += " " + eventResult.message;

    // Envoi de la réponse de la requète
    return res.json({
      message,
    });
  } catch (err) {
    next(err);
  }
};

/* Suppression d'un résultat */
exports.deleteResult = async (req, res, next) => {
  try {
    // Récupération du paramètre de la requète
    let resultId = parseInt(req.params.id);

    // Vérification de l'existance du paramètre
    if (!resultId) {
      throw new RequestError("Id du résultat manquant.");
    }

    // Suppression de l'résultat dans la base de données
    await Result.destroy({ where: { id: resultId }, force: true });

    // Envoi de la réponse de la requète
    return res.status(204).json();
  } catch (err) {
    next(err);
  }
};
