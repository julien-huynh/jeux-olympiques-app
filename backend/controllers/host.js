const DB = require("../db.config");
const slugify = require("slugify");
const { Op } = require("sequelize");
const Host = DB.Host;
const Event = DB.Event;

const { RequestError, HostError } = require("../error/customError");

/* Récupération de tous les hôtes */
exports.getAllHosts = (req, res, next) => {
  // Récupération des hôtes
  Host.findAll({ include: Event })
    .then((hosts) => res.json({ data: hosts }))
    .catch((err) => next(err));
};

/* Récupération d'un hôte en fonction de l'Id*/
exports.getHostById = async (req, res, next) => {
  try {
    // Récupération du paramètre
    let hostId = req.params.id;

    // Vérification de l'existance du paramètre
    if (!hostId) {
      throw new RequestError("Id manquant.");
    }

    // Récupération du hôte
    let host = await Host.findOne({
      where: { id: hostId },
      include: [{ model: Event }],
    });

    // Vérification de l'existance du hôte
    if (host === null) {
      throw new HostError("Cet hôte n'existe pas .", 0);
    }

    // Réponse de la requète
    return res.json({ data: host });
  } catch (err) {
    next(err);
  }
};

/* Récupération d'un hôte en fonction du slug */
exports.getHostBySlug = async (req, res, next) => {
  try {
    // Récupération du paramètre
    let hostSlug = req.params.slug;

    // Vérification de l'existance du paramètre
    if (!hostSlug) {
      throw new RequestError("Slug manquant.");
    }

    // Récupération du hôte
    let host = await Host.findOne({
      where: { slug: hostSlug },
      include: [{ model: Event }],
    });

    // Vérification de l'existance du hôte
    if (host === null) {
      throw new HostError("Cet hôte n'existe pas .", 0);
    }

    // Réponse de la requète
    return res.json({ data: host });
  } catch (err) {
    next(err);
  }
};

/* Ajout d'un hôte */
exports.addHost = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    const { name } = req.body;

    // Vérification de l'existance des paramètres
    if (!name) {
      throw new RequestError("Paramètre(s) manquant(s).");
    }

    // Récupération du hôte du même nom
    let host = await Host.findOne({ where: { name: name }, raw: true });

    // Vérification de l'existance du hôte du même nom
    if (host !== null) {
      throw new RequestError(
        `L'hôte ${name} existe déja dans la base de données.`,
        1
      );
    }

    // Génération du slug avec le nom du hôte
    req.body.slug = slugify(name);

    // Création du hôte dans la base de données
    let hostc = await Host.create(req.body);

    // Envoi de la réponse de la requète
    return res.json({
      message: "L'hôte à bien été créée .",
      data: hostc,
    });
  } catch (err) {
    next(err);
  }
};

/* Modification d'un hôte */
exports.updateHost = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    let hostId = parseInt(req.params.id);
    const { name } = req.body;

    // Vérification de l'existance du paramètre
    if (!hostId) {
      throw new RequestError("Id du hôte manquant.");
    }

    // Récupération du hôte
    let host = await Host.findOne({ where: { id: hostId }, raw: true });

    // Vérification de l'existance du hôte
    if (host === null) {
      throw new HostError("Cet hôte n'existe pas .", 0);
    }

    if (name) {
      // Récupération du hôte du même nom
      host = await Host.findOne({
        where: { name: name, id: { [Op.ne]: hostId } },
        raw: true,
      });

      // Vérification de l'existance du hôte de la même couleur
      if (host !== null) {
        throw new RequestError(
          `L'hôte ${name} existe déja dans la base de données.`,
          1
        );
      }
      // Génération du slug avec le nom du hôte
      req.body.slug = slugify(name);
    }

    // Modification du hôte dans la base de données
    await Host.update(req.body, { where: { id: hostId } });

    // Envoi de la réponse de la requète
    return res.json({
      message: "L'hôte à bien été modifié .",
    });
  } catch (err) {
    next(err);
  }
};

// exports.untrashHost = async (req, res, next) => {
// 	try {
// 		let hostId = parseInt(req.params.id);

// 		if (!hostId) {
// 			throw new RequestError("Paramètre(s) manquant(s) .");
// 		}

// 		await Host.restore({ where: { id: hostId } });

// 		return res.status(204).json({});
// 	} catch (err) {
// 		next(err);
// 	}
// };

// exports.trashHost = async (req, res, next) => {
// 	try {
// 		let hostId = parseInt(req.params.id);

// 		if (!hostId) {
// 			throw new RequestError("Paramètre(s) manquant(s) .");
// 		}

// 		await Host.destroy({ where: { id: hostId } });

// 		return res.status(204).json({});
// 	} catch (err) {
// 		next(err);
// 	}
// };

/* Suppression d'un hôte */
exports.deleteHost = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    let hostId = parseInt(req.params.id);

    // Vérification de l'existance du paramètre
    if (!hostId) {
      throw new RequestError("Id du hôte manquant.");
    }

    // Suppression de la hôte dans la base de données
    await Host.destroy({ where: { id: hostId }, force: true });

    // Envoi de la réponse de la requète
    return res.status(204).json({});
  } catch (err) {
    next(err);
  }
};
