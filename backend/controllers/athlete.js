/* Import des modules nécéssaires */
const DB = require("../db.config");
const slugify = require("slugify");
const { Op } = require("sequelize");
const Athlete = DB.Athlete;
const Result = DB.Result;
const Country = DB.Country;
const Sport = DB.Sport;
const Event = DB.Event;
const EventType = DB.EventType;
const Category = DB.Category;
const Host = DB.Host;
const Medal = DB.Medal;
const { RequestError, AthleteError } = require("../error/customError");
const { addAssociation } = require("../_utils/associationUtils");
const fs = require("fs");
const path = require("path");

/* Récupération de tous les athlètes, leurs résultat, leur pays*/
exports.getAllAthletes = (req, res, next) => {
  // Récupération des athlètes
  Athlete.findAll({
    include: [{ model: Country }],
  })
    .then((atheletes) => res.json({ data: atheletes }))
    .catch((err) => next(err));
};

/* Récupération d'un athlète, ses médailles, son pays, son sport en fonction de l'ID*/
exports.getAthleteById = async (req, res, next) => {
  try {
    // Récupération du paramètre
    let athleteId = parseInt(req.params.id);

    // Vérification de l'existance du paramètre
    if (!athleteId) {
      throw new RequestError("Id manquant.");
    }

    // Récupération de l'athlète
    let athlete = await Athlete.findOne({
      where: { id: athleteId },
      include: [
        {
          model: Result,
          include: [
            {
              model: Event,
              include: [
                { model: Sport },
                { model: EventType },
                { model: Category },
                { model: Host },
              ],
            },
            { model: Medal },
          ],
        },
        { model: Country },
      ],
    });

    // Vérification de l'existance de l'athlète
    if (athlete === null) {
      throw new AthleteError("Cet athlete n'existe pas .", 0);
    }

    // Réponse de la requète
    return res.json({ data: athlete });
  } catch (err) {
    next(err);
  }
};

/* Récupération d'un athlète, ses médailles, son pays, son sport en fonction du slug */
exports.getAthleteBySlug = async (req, res, next) => {
  try {
    // Récupération du paramètre
    let athleteSlug = req.params.slug;

    // Vérification de l'existance du paramètre
    if (!athleteSlug) {
      throw new RequestError("Slug manquant.");
    }

    // Récupération de l'athlète
    let athlete = await Athlete.findOne({
      where: { slug: athleteSlug },
      include: [
        {
          model: Result,
          include: [
            {
              model: Event,
              include: [
                { model: Sport },
                { model: EventType },
                { model: Category },
                { model: Host },
              ],
            },
            { model: Medal },
          ],
        },
        { model: Country },
      ],
    });

    // Vérification de l'existance de l'athlète
    if (athlete === null) {
      throw new AthleteError("Cet athlete n'existe pas .", 0);
    }

    // Réponse de la requète
    return res.json({ data: athlete });
  } catch (err) {
    next(err);
  }
};

/* Ajout d'un athlète, avec son pays, et son sport */
exports.addAthlete = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    const { lastname, firstname, age, height, weight, countryId } =
      req.body;

      // Récupération de l'image
      req.body.image = req.file ? req.file.path : null;

    // Vérification de l'existance des paramètres
    if (!lastname || !firstname) {
      throw new RequestError("Paramètre(s) manquant(s) .");
    }

    const whereClause = {
      firstname: firstname,
      lastname: lastname,
    };

    // Récupération des paramètres entrés
    if (countryId) {
      whereClause.CountryId = countryId;
    }

    // Récupération d'un athlète du même nom
    let athlete = await Athlete.findOne({
      where: whereClause,
      raw: true,
    });

    // Vérification de l'existance d'un athlète du même nom
    if (athlete !== null) {
      throw new RequestError(
        `Un athlète avec ce nom et prénom existe déja dans la base de données.`,
        1
      );
    }

    // Génération du nom complet de l'athlète
    let fullname = firstname + " " + lastname;

    // Génération du slug avec le nom complet de l'athlète
    req.body.slug = slugify(fullname.toLowerCase());

    // Création de l'athlète dans la base de données
    let athletec = await Athlete.create(req.body, {
      include: [{ model: Country }],
    });

    //Si un pays a été donné en paramètres
    const countryResult = await addAssociation(
      countryId,
      Country,
      "addAthlete",
      "Le pays spécifié n'existe pas.",
      athletec
    );

    // Preparation du message de réponse
    let message = "L'athlète a bien été créé.";
    if (!countryResult.success) message += " " + countryResult.message;

    // Envoi de la réponse de la requète
    return res.status(201).json({
      message: message,
    });
  } catch (err) {
    next(err);
  }
};

/* Modification d'un athlète, avec son pays, et son sport */
exports.updateAthlete = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    let athleteId = parseInt(req.params.id);
    const { lastname, firstname, age, height, weight, countryId } =
      req.body;

    // Récupération de l'image
    const image = req.file ? req.file.path : undefined;

    // Vérification de l'existance du paramètre
    if (!athleteId) {
      throw new RequestError("Id de l'athlète manquant.");
    }

    // Récupération de l'athlète
    let athlete = await Athlete.findOne({
      where: { id: athleteId },
      raw: true,
    });

    // Vérification de l'existance de l'athlète
    if (athlete === null) {
      throw new AthleteError("Cet athlète n'existe pas .", 0);
    }

    if (image) {
      // Supprimer l'ancienne image si elle existe
      if (athlete.image) {
        fs.unlinkSync(path.resolve(athlete.image));
      }
      req.body.image = image;
    }

    const whereClause = {
      firstname: athlete.firstname,
      lastname: athlete.lastname,
      id: { [Op.ne]: athleteId },
    };

    // Récupération des noms entrés en paramètre
    if (firstname) {
      whereClause.firstname = firstname;
    }
    if (lastname) {
      whereClause.lastname = lastname;
    }

    // Recherche de l'athlète en fonction des critères
    athlete = await Athlete.findOne({
      where: whereClause,
      raw: true,
    });

    // Vérification de l'existance d'un athlète du même nom
    if (athlete !== null) {
      throw new AthleteError(
        `Un autre athlète avec ce nom et prénom existe déja dans la base de données.`,
        1
      );
    }

    // Génération du slug avec le nom complet de l'athlète
    req.body.slug = slugify(`${whereClause.firstname} ${whereClause.lastname}`);

    // Modification de l'athlète dans la base de données
    let athleteu = await Athlete.update(req.body, { where: { id: athleteId } });

    //Si un pays a été donné en paramètres
    const countryResult = await addAssociation(
      countryId,
      Country,
      "addAthlete",
      "Le pays spécifié n'existe pas.",
      athleteu
    );

    // Preparation du message de réponse
    let message = "L'athlète a bien été modifié.";
    if (!countryResult.success) message += " " + countryResult.message;

    // Envoi de la réponse de la requète
    return res.status(200).json({
      message: message,
    });
  } catch (err) {
    next(err);
  }
};

// exports.untrashAthlete = async (req, res, next) => {
// 	try {
// 		let athleteId = parseInt(req.params.id);

// 		if (!athleteId) {
// 			throw new RequestError("Paramètre(s) manquant(s) .");
// 		}

// 		await Athlete.restore({ where: { id: athleteId } });

// 		return res.status(204).json({});
// 	} catch (err) {
// 		next(err);
// 	}
// };

// exports.trashAthlete = async (req, res, next) => {
// 	try {
// 		let athleteId = parseInt(req.params.id);

// 		if (!athleteId) {
// 			throw new RequestError("Paramètre(s) manquant(s) .");
// 		}

// 		await Athlete.destroy({ where: { id: athleteId } });

// 		return res.status(204).json({});
// 	} catch (err) {
// 		next(err);
// 	}
// };

/* Suppression d'un athlète */
exports.deleteAthlete = async (req, res, next) => {
  try {
    // Récupération du paramètre de la requète
    let athleteId = parseInt(req.params.id);

    // Vérification de l'existance du paramètre
    if (!athleteId) {
      throw new RequestError("Id de l'athlète manquant.");
    }

    // Suppression de l'athlète dans la base de données
    await Athlete.destroy({ where: { id: athleteId }, force: true });

    // Envoi de la réponse de la requète
    return res.status(204).json();
  } catch (err) {
    next(err);
  }
};
