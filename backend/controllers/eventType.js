/* Import des modules nécéssaires */
const DB = require("../db.config");
const slugify = require("slugify");
const { Op } = require("sequelize");
const Event = DB.Event;
const Sport = DB.Sport;
const EventType = DB.EventType;

const { RequestError, EventTypeError } = require("../error/customError");

/* Récupération de tous les types d'evenements */
exports.getAllEventTypes = (req, res, next) => {
  // Récupération des types d'evenements
  EventType.findAll()
    .then((eventTypes) => res.json({ data: eventTypes }))
    .catch((err) => next(err));
};

/* Récupération d'un type d'evenements en fonction de l'Id */
exports.getEventTypeById = async (req, res, next) => {
  try {
    // Récupération du paramètre
    let eventTypeId = parseInt(req.params.id);

    // Vérification de l'existance du paramètre
    if (!eventTypeId) {
      throw new RequestError("Paramètre(s) manquant(s) .");
    }

    // Récupération du type d'evenements
    let eventType = await EventType.findOne({
      where: { id: eventTypeId },
      include: [{ model: Event, include: { model: Sport } }],
    });

    // Vérification de l'existance du type d'evenements
    if (eventType === null) {
      throw new EventTypeError("Ce type d'evenements n'existe pas .", 0);
    }

    // Réponse de la requète
    return res.json({ data: eventType });
  } catch (err) {
    next(err);
  }
};

/* Récupération d'un type d'evenements en fonction de l'Id */
exports.getEventTypeByName = async (req, res, next) => {
  try {
    // Récupération du paramètre
    let eventTypeName = req.params.name;

    // Vérification de l'existance du paramètre
    if (!eventTypeName) {
      throw new RequestError("Paramètre(s) manquant(s) .");
    }

    // Récupération du type d'evenements
    let eventType = await EventType.findOne({
      where: { name: eventTypeName },
      include: [{ model: Event, include: { model: Sport } }],
    });

    // Vérification de l'existance du type d'evenements
    if (eventType === null) {
      throw new EventTypeError("Ce type d'evenements n'existe pas .", 0);
    }

    // Réponse de la requète
    return res.json({ data: eventType });
  } catch (err) {
    next(err);
  }
};

/* Ajout d'un type d'evenements */
exports.addEventType = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    const { name, image } = req.body;

    // Vérification de l'existance des paramètres
    if (!name) {
      throw new RequestError("Paramètre(s) manquant(s) .");
    }

    // Récupération du type d'evenements du même nom
    let eventType = await EventType.findOne({
      where: { name: name },
      raw: true,
    });

    // Vérification de l'existance du type d'evenements du même nom
    if (eventType !== null) {
      throw new RequestError(
        `Le type d'evenements ${name} existe déja dans la base de données.`,
        1
      );
    }

    // Génération du slug avec le nom du type d'evenements
    req.body.slug = slugify(name);

    // Création du type d'evenements dans la base de données
    let eventTypec = await EventType.create(req.body);

    // Envoi de la réponse de la requète
    return res.json({
      message: "Le type d'evenements à bien été créé .",
      data: eventTypec,
    });
  } catch (err) {
    next(err);
  }
};

/* Modification d'un type d'evenements */
exports.updateEventType = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    let eventTypeId = parseInt(req.params.id);
    const { name, image } = req.body;

    // Vérification de l'existance du paramètre
    if (!eventTypeId) {
      throw new RequestError("ID du type d'evenements manquant.");
    }

    // Récupération du type d'evenements
    let eventType = await EventType.findOne({
      where: { id: eventTypeId },
      raw: true,
    });

    // Vérification de l'existance du type d'evenements
    if (eventType === null) {
      throw new EventTypeError("Ce type d'evenements n'existe pas .", 0);
    }

    // Si un nom a été donné en paramètres
    if (name) {
      // Vérification de l'existance du type d'evenements avec le nom donné en paramètre
      eventType = await EventType.findOne({
        where: { name: name, id: { [Op.ne]: eventTypeId } },
        raw: true,
      });
      if (eventType !== null) {
        throw new RequestError(
          `Le type d'evenements ${name} existe déja dans la base de données.`,
          1
        );
      }
      req.body.slug = slugify(req.body.name);
    }

    // Modification du type d'evenements dans la base de données
    await EventType.update(req.body, { where: { id: eventTypeId } });

    // Envoi de la réponse de la requète
    return res.json({
      message: "Le type d'evenements à bien été modifié .",
    });
  } catch (err) {
    next(err);
  }
};

// exports.untrashSport = async (req, res, next) => {
// 	try {
// 		let eventTypeId = parseInt(req.params.id);

// 		if (!eventTypeId) {
// 			throw new RequestError("Paramètre(s) manquant(s) .");
// 		}

// 		await Sport.restore({ where: { id: eventTypeId } });

// 		return res.status(204).json({});
// 	} catch (err) {
// 		next(err);
// 	}
// };

// exports.trashSport = async (req, res, next) => {
// 	try {
// 		let eventTypeId = parseInt(req.params.id);

// 		if (!eventTypeId) {
// 			throw new RequestError("Paramètre(s) manquant(s) .");
// 		}

// 		await Sport.destroy({ where: { id: eventTypeId } });

// 		return res.status(204).json({});
// 	} catch (err) {
// 		next(err);
// 	}
// };

/* Suppression d'un type d'evenements */
exports.deleteEventType = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    let eventTypeID = parseInt(req.params.id);

    // Vérification de l'existance du paramètre
    if (!eventTypeID) {
      throw new RequestError("ID du type d'evenements manquant.");
    }

    // Suppression du type d'evenements dans la base de données
    await EventType.destroy({ where: { id: eventTypeID }, force: true });

    // Envoi de la réponse de la requète
    return res.status(204).json({});
  } catch (err) {
    next(err);
  }
};
