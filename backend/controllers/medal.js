const DB = require("../db.config");
const { Op } = require("sequelize");
const Medal = DB.Medal;
const Result = DB.Result;

const { RequestError, MedalError } = require("../error/customError");

/* Récupération de toutes les médailles */
exports.getAllMedals = (req, res, next) => {
  // Récupération des médailles
  Medal.findAll({ include: Result })
    .then((medals) => res.json({ data: medals }))
    .catch((err) => next(err));
};

/* Récupération d'une médaille en fonction de l'Id*/
exports.getMedalById = async (req, res, next) => {
  try {
    // Récupération du paramètre
    let medalId = req.params.id;

    // Vérification de l'existance du paramètre
    if (!medalId) {
      throw new RequestError("Id manquant.");
    }

    // Récupération de la médaille
    let medal = await Medal.findOne({
      where: { id: medalId },
      include: [{ model: Result }],
    });

    // Vérification de l'existance de la médaille
    if (medal === null) {
      throw new MedalError("Cette médaille n'existe pas .", 0);
    }

    // Réponse de la requète
    return res.json({ data: medal });
  } catch (err) {
    next(err);
  }
};

/* Récupération d'une médaille en fonction de la couleur */
exports.getMedalByColor = async (req, res, next) => {
  try {
    // Récupération du paramètre
    let medalColor = req.params.color;

    // Vérification de l'existance du paramètre
    if (!medalColor) {
      throw new RequestError("Color manquant.");
    }

    // Récupération de la médaille
    let medal = await Medal.findOne({
      where: { color: medalColor },
      include: [{ model: Result }],
    });

    // Vérification de l'existance de la médaille
    if (medal === null) {
      throw new MedalError("Cette médaille n'existe pas .", 0);
    }

    // Réponse de la requète
    return res.json({ data: medal });
  } catch (err) {
    next(err);
  }
};

/* Ajout d'une médaille */
exports.addMedal = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    const { color } = req.body;

    // Vérification de l'existance des paramètres
    if (!color) {
      throw new RequestError("Paramètre(s) manquant(s).");
    }

    // Récupération de la médaille de la même couleur
    let medal = await Medal.findOne({ where: { color: color }, raw: true });

    // Vérification de l'existance de la médaille de la même couleur
    if (medal !== null) {
      throw new RequestError(
        `La médaille ${color} existe déja dans la base de données.`,
        1
      );
    }

    // Création de la médaille dans la base de données
    let medalc = await Medal.create(req.body);

    // Envoi de la réponse de la requète
    return res.json({
      message: "La médaille à bien été créée .",
      data: medalc,
    });
  } catch (err) {
    next(err);
  }
};

/* Modification d'une médaille */
exports.updateMedal = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    let medalId = parseInt(req.params.id);
    const { color } = req.body;

    // Vérification de l'existance du paramètre
    if (!medalId) {
      throw new RequestError("Id de la médaille manquant.");
    }

    // Récupération de la médaille
    let medal = await Medal.findOne({ where: { id: medalId }, raw: true });

    // Vérification de l'existance de la médaille
    if (medal === null) {
      throw new MedalError("Cette médaille n'existe pas .", 0);
    }

    // Récupération de la médaille de la même couleur
    medal = await Medal.findOne({
      where: { color: color, id: { [Op.ne]: medalId } },
      raw: true,
    });

    // Vérification de l'existance de la médaille de la même couleur
    if (medal !== null) {
      throw new RequestError(
        `La médaille ${color} existe déja dans la base de données.`,
        1
      );
    }

    // Modification de la médaille dans la base de données
    await Medal.update(req.body, { where: { id: medalId } });

    // Envoi de la réponse de la requète
    return res.json({
      message: "La médaille à bien été modifié .",
    });
  } catch (err) {
    next(err);
  }
};

// exports.untrashMedal = async (req, res, next) => {
// 	try {
// 		let medalId = parseInt(req.params.id);

// 		if (!medalId) {
// 			throw new RequestError("Paramètre(s) manquant(s) .");
// 		}

// 		await Medal.restore({ where: { id: medalId } });

// 		return res.status(204).json({});
// 	} catch (err) {
// 		next(err);
// 	}
// };

// exports.trashMedal = async (req, res, next) => {
// 	try {
// 		let medalId = parseInt(req.params.id);

// 		if (!medalId) {
// 			throw new RequestError("Paramètre(s) manquant(s) .");
// 		}

// 		await Medal.destroy({ where: { id: medalId } });

// 		return res.status(204).json({});
// 	} catch (err) {
// 		next(err);
// 	}
// };

/* Suppression d'une médaille */
exports.deleteMedal = async (req, res, next) => {
  try {
    // Récupération des paramètres de la requète
    let medalId = parseInt(req.params.id);

    // Vérification de l'existance du paramètre
    if (!medalId) {
      throw new RequestError("Id de la médaille manquant.");
    }

    // Suppression de la médaille dans la base de données
    await Medal.destroy({ where: { id: medalId }, force: true });

    // Envoi de la réponse de la requète
    return res.status(204).json({});
  } catch (err) {
    next(err);
  }
};
