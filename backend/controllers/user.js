const DB = require("../db.config");
const slugify = require("slugify");
const { Op } = require("sequelize");
const User = DB.User;
const { RequestError, UserError } = require("../error/customError");

exports.getAllUsers = (req, res, next) => {
  User.findAll()
    .then((users) => res.json({ data: users }))
    .catch((err) => next(err));
};

exports.getUser = async (req, res, next) => {
  try {
    let userId = parseInt(req.params.id);

    if (!userId) {
      throw new RequestError("Paramètre(s) manquant(s) .");
    }

    let user = await User.findOne({
      where: { id: userId },
    });

    if (user === null) {
      throw new UserError("Cet utilisateur n'existe pas .", 0);
    }

    return res.json({ data: user });
  } catch (err) {
    next(err);
  }
};

exports.getMyProfile = async (req, res, next) => {
  try {
    let userId = req.decodedToken.id;

    let user = await User.findOne({
      where: { id: userId },
      attributes: [
        "id",
        "name",
        "slug",
        "email",
        "firstname",
        "username",
        "telephone",
      ],
    });

    if (user === null) {
      throw new UserError("Not logged in ?", 0);
    }

    return res.json({ data: user });
  } catch (err) {
    next(err);
  }
};

exports.addUser = async (req, res, next) => {
  try {
    const { name, firstname, username, email, password, roles } = req.body;

    if (!name || !firstname || !username || !email || !password || !roles) {
      throw new RequestError("Paramètre(s) manquant(s) .");
    }

    let user = await User.findOne({ where: { email: email }, raw: true });

    if (user !== null) {
      throw new RequestError(`L'adresse email ${email} est déjà utilisée.`, 1);
    }

    user = await User.findOne({ where: { username: username }, raw: true });

    if (user !== null) {
      throw new RequestError(`Le pseudo ${username} est déjà utilisé.`, 1);
    }

    if (roles !== "ROLE_ADMIN" && roles !== "ROLE_USER") {
      throw new RequestError(`Le format du rôle est incohérent.`, 1);
    }

    req.body.slug = slugify(username);

    let userc = await User.create(req.body);

    return res.json({
      message: "L'utilisateur à bien été créé .",
      data: userc,
    });
  } catch (err) {
    next(err);
  }
};

exports.updateMyProfile = async (req, res, next) => {
  try {
    const { username, email } = req.body;

    if (username) {
      if (username !== req.decodedToken.username) {
        let user = await User.findOne({
          where: { username: username },
          raw: true,
        });
        if (user !== null) {
          throw new UserError(`Le pseudo ${username} est déjà utilisé .`, 0);
        }
      }
    }
    if (email) {
      if (email !== req.decodedToken.email) {
        let user = await User.findOne({ where: { email: email }, raw: true });
        if (user !== null) {
          throw new UserError(
            `L'adresse e-mail ${email} est déjà utilisée .`,
            0
          );
        }
      }
    }
    req.body.roles = req.decodedToken.roles;

    await User.update(req.body, { where: { id: req.decodedToken.id } });

    return res.json({
      message: "Votre profil à bien été modifié .",
    });
  } catch (err) {
    next(err);
  }
};

exports.updateUser = async (req, res, next) => {
  try {
    let userId = parseInt(req.params.id);
    const { username, email, roles } = req.body;

    if (!userId) {
      throw new RequestError("Paramètre(s) manquant(s) .");
    }

    let user = await User.findOne({ where: { id: userId }, raw: true });

    if (user === null) {
      throw new UserError("Cet utilisateur n'existe pas .", 0);
    }

    if (username) {
      user = await User.findOne({
        where: { username: username, id: { [Op.ne]: userId } },
        raw: true,
      });
      if (user === null) {
        throw new UserError(`Le pseudo ${username} est déjà utilisé .`, 0);
      }
    }

    if (email) {
      user = await User.findOne({
        where: { email: email, id: { [Op.ne]: userId } },
        raw: true,
      });
      if (user === null) {
        throw new UserError(`L'adresse e-mail ${email} est déjà utilisée .`, 0);
      }
    }

    if (roles !== "ROLE_ADMIN" && roles !== "ROLE_USER") {
      throw new RequestError(`Le format du rôle est incohérent.`, 1);
    }

    req.body.slug = slugify(req.body.username);

    await User.update(req.body, { where: { id: userId } });

    return res.json({
      message: "L'utilisateur à bien été modifié .",
    });
  } catch (err) {
    next(err);
  }
};

exports.untrashUser = async (req, res, next) => {
  try {
    let userId = parseInt(req.params.id);

    if (!userId) {
      throw new RequestError("Paramètre(s) manquant(s) .");
    }

    await User.restore({ where: { id: userId } });

    return res.status(204).json({});
  } catch (err) {
    next(err);
  }
};

exports.trashUser = async (req, res, next) => {
  try {
    let userId = parseInt(req.params.id);

    if (!userId) {
      throw new RequestError("Paramètre(s) manquant(s) .");
    }

    await User.destroy({ where: { id: userId } });

    return res.status(204).json({});
  } catch (err) {
    next(err);
  }
};

exports.deleteUser = async (req, res, next) => {
  try {
    let userId = parseInt(req.params.id);

    if (!userId) {
      throw new RequestError("Paramètre(s) manquant(s) .");
    }

    await User.destroy({ where: { id: userId }, force: true });

    return res.status(204).json({});
  } catch (err) {
    next(err);
  }
};
