const express = require("express");
const userController = require("../controllers/user");
const check = require("../jsonwebtoken/check");

let router = express.Router();

router.use((req, res, next) => {
	const event = new Date();
	console.log("USER Time", event.toString());
	next();
});

router.get("/", userController.getAllUsers);

router.get("/:id", userController.getUser);

router.get("/me", check.checkUserToken, userController.getMyProfile);

router.put("", userController.addUser);

router.patch("/me", check.checkUserToken, userController.updateMyProfile);

router.patch("/:id", userController.updateUser);

// router.post("/untrash/:id", check.checkAdminToken, userController.untrashUser);

// router.delete("/trash/:id", check.checkAdminToken, userController.trashUser);

router.delete("/:id", userController.deleteUser);

module.exports = router;
