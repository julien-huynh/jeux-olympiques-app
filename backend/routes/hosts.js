const express = require("express");
const hostController = require("../controllers/host");

let router = express.Router();

router.use((req, res, next) => {
  const event = new Date();
  console.log("Host Time", event.toString());
  next();
});

router.get("/", hostController.getAllHosts);

router.get("/:id", hostController.getHostById);

router.get("/slug/:slug", hostController.getHostBySlug);

router.put("", hostController.addHost);

router.patch("/:id", hostController.updateHost);

// router.post("/untrash/:id", check.checkAdminToken, hostController.untrashHost);

// router.delete("/trash/:id", check.checkAdminToken, hostController.trashHost);

router.delete("/:id", hostController.deleteHost);

module.exports = router;
