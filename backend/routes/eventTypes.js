const express = require("express");
const eventTypeController = require("../controllers/eventType");

let router = express.Router();

router.use((req, res, next) => {
  const event = new Date();
  console.log("EventType Time", event.toString());
  next();
});

router.get("/", eventTypeController.getAllEventTypes);

router.get("/:id", eventTypeController.getEventTypeById);

router.get("/name/:name", eventTypeController.getEventTypeByName);

router.put("", eventTypeController.addEventType);

router.patch("/:id", eventTypeController.updateEventType);

// router.post("/untrash/:id", check.checkAdminToken, eventTypeController.untrashEventType);

// router.delete("/trash/:id", check.checkAdminToken, eventTypeController.trashEventType);

router.delete("/:id", eventTypeController.deleteEventType);

module.exports = router;
