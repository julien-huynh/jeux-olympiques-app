const express = require("express");
const categoryController = require("../controllers/category");

let router = express.Router();

router.use((req, res, next) => {
  const event = new Date();
  console.log("Category Time", event.toString());
  next();
});

router.get("/", categoryController.getAllCategories);

router.get("/:id", categoryController.getCategoryById);

router.get("/name/:name", categoryController.getCategoryByName);

router.put("", categoryController.addCategory);

router.patch("/:id", categoryController.updateCategory);

// router.post("/untrash/:id", check.checkAdminToken, categoryController.untrashCategory);

// router.delete("/trash/:id", check.checkAdminToken, categoryController.trashCategory);

router.delete("/:id", categoryController.deleteCategory);

module.exports = router;
