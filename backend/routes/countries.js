const express = require("express");
const countryController = require("../controllers/country");

let router = express.Router();

router.use((req, res, next) => {
  const event = new Date();
  console.log("Country Time", event.toString());
  next();
});

router.get("/", countryController.getAllCountries);

router.get("/:id", countryController.getCountryById);

router.get("/name/:name", countryController.getCountryByName);

router.get("/code/:code", countryController.getCountryByCode);

router.put("", countryController.addCountry);

router.patch("/:id", countryController.updateCountry);

// router.post("/untrash/:id", check.checkAdminToken, countryController.untrashCountry);

// router.delete("/trash/:id", check.checkAdminToken, countryController.trashCountry);

router.delete("/:id", countryController.deleteCountry);

module.exports = router;
