const express = require("express");
const resultController = require("../controllers/result");

let router = express.Router();

router.use((req, res, next) => {
  const result = new Date();
  console.log("Result Time", result.toString());
  next();
});

router.get("/", resultController.getAllResults);

router.get("/:id", resultController.getResultById);

router.put("", resultController.addResult);

router.patch("/:id", resultController.updateResult);

// router.post("/untrash/:id", check.checkAdminToken, resultController.untrashResult);

// router.delete("/trash/:id", check.checkAdminToken, resultController.trashResult);

router.delete("/:id", resultController.deleteResult);

module.exports = router;
