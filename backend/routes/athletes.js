const express = require("express");
const athleteController = require("../controllers/athlete");
const upload = require('../multer/multer');

let router = express.Router();

router.use((req, res, next) => {
	const event = new Date();
	console.log("Athlete Time", event.toString());
	next();
});

router.get("/", athleteController.getAllAthletes);

router.get("/:id", athleteController.getAthleteById);

router.get("/slug/:slug", athleteController.getAthleteBySlug);

router.put("", upload.single('image'), athleteController.addAthlete);

router.patch("/:id", upload.single('image'), athleteController.updateAthlete);

// router.post("/untrash/:id", check.checkAdminToken, athleteController.untrashAthlete);

// router.delete("/trash/:id", check.checkAdminToken, athleteController.trashAthlete);

router.delete("/:id", athleteController.deleteAthlete);


module.exports = router;
