const express = require("express");
const medalController = require("../controllers/medal");

let router = express.Router();

router.use((req, res, next) => {
	const event = new Date();
	console.log("Medal Time", event.toString());
	next();
});

router.get("/", medalController.getAllMedals);

router.get("/:id", medalController.getMedalById);

router.get("/color/:color", medalController.getMedalByColor);

router.put("", medalController.addMedal);

router.patch("/:id", medalController.updateMedal);

// router.post("/untrash/:id", check.checkAdminToken, medalController.untrashMedal);

// router.delete("/trash/:id", check.checkAdminToken, medalController.trashMedal);

router.delete("/:id", medalController.deleteMedal);

module.exports = router;
