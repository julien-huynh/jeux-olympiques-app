const express = require("express");
const eventController = require("../controllers/event");

let router = express.Router();

router.use((req, res, next) => {
  const event = new Date();
  console.log("Event Time", event.toString());
  next();
});

router.get("/", eventController.getAllEvents);

router.get("/params/", eventController.getEventByParams);

router.get("/:id", eventController.getEventById);

router.put("", eventController.addEvent);

router.patch("/:id", eventController.updateEvent);

// router.post("/untrash/:id", check.checkAdminToken, eventController.untrashEvent);

// router.delete("/trash/:id", check.checkAdminToken, eventController.trashEvent);

router.delete("/:id", eventController.deleteEvent);

module.exports = router;
