const express = require("express");
const sportController = require("../controllers/sport");

let router = express.Router();

router.use((req, res, next) => {
  const event = new Date();
  console.log("Sport Time", event.toString());
  next();
});

router.get("/", sportController.getAllSports);

router.get("/:id", sportController.getSportById);

router.get("/name/:name", sportController.getSportByName);

router.put("", sportController.addSport);

router.patch("/:id", sportController.updateSport);

// router.post("/untrash/:id", check.checkAdminToken, sportController.untrashSport);

// router.delete("/trash/:id", check.checkAdminToken, sportController.trashSport);

router.delete("/:id", sportController.deleteSport);

module.exports = router;
