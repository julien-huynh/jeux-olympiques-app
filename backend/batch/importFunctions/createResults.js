const axios = require('axios');
const {generateSlug} = require("../../_utils/associationUtils");

exports.createResults = async (rows) => {
    let count = 0;

    for (const row of rows) {
        try {
            const sport = await axios.get(`http://localhost:8245/sport/name/${row.discipline_title}`);
            const eventType = await axios.get(`http://localhost:8245/eventType/name/${row.event_title}`);
            const host = await axios.get(`http://localhost:8245/host/slug/${row.slug_game}`);
            const category = await axios.get(`http://localhost:8245/category/name/${row.event_gender}`);

            const medal = await axios.get(`http://localhost:8245/medal/color/${row.medal_type}`);
            const athlete = await axios.get(`http://localhost:8245/athlete/slug/${generateSlug(row.athlete_full_name)}`);
            const event = await axios.get('http://localhost:8245/event/params', {
                params: {
                    sportId: sport.data.data.id,
                    eventTypeId: eventType.data.data.id,
                    categoryId: category.data.data.id,
                    hostId: host.data.data.id
                }
            })

            await axios.put("http://localhost:8245/result",
                {
                    medalId: medal.data.data.id,
                    athleteId: athlete.data.data.id,
                    eventId: event.data.data.id,
                }
            );

            count++;
            console.log(`Successfully created result`, count);
        } catch (error) {
            count++;
            console.error(`Error adding result: ${error.message} - Le résultat a déjà été ajouté ou correspond à un sport d'équipe`, count);
        }
    }

}
