const axios = require('axios');
const {generateSlug} = require("../../_utils/associationUtils");

exports.importEventTypes = async (rows) => {
    let count = 0;

    for (const row of rows) {
        try {
            if (row.event_title !== "" && row.event_title !== null) {
                await axios.put("http://localhost:8245/eventType",
                    {
                        name: row.event_title,
                        slug: generateSlug(row.event_title),
                        image: null
                    }
                );

                count++;
                console.log(`Successfully added event type: ${row.event_title}`, count);
            }
        } catch (error) {
            count++;
            console.error(`Error adding event type: ${error.message} - ${row.event_title} a déjà été ajouté`, count);
        }
    }

}
