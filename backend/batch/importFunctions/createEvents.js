const axios = require('axios');

exports.createEvents = async (rows) => {
    let count = 0;

    for (const row of rows) {
        try {
            var sport = await axios.get(`http://localhost:8245/sport/name/${row.discipline_title}`);
            var eventType = await axios.get(`http://localhost:8245/eventType/name/${row.event_title}`);
            var host = await axios.get(`http://localhost:8245/host/slug/${row.slug_game}`);
            var category = await axios.get(`http://localhost:8245/category/name/${row.event_gender}`);

            await axios.put("http://localhost:8245/event",
                {
                    date: null,
                    sportId: sport.data.data.id,
                    eventTypeId: eventType.data.data.id,
                    categoryId: category.data.data.id,
                    hostId: host.data.data.id,
                }
            );

            count++;
            console.log(`Successfully created event: ${sport.data.data.id}/${category.data.data.id}/${eventType.data.data.id}/${host.data.data.id}`, count);
        } catch (error) {
            count++;
            console.error(`Error adding event: ${error.message} - ${sport.data.data.id}/${category.data.data.id}/${eventType.data.data.id}/${host.data.data.id} a déjà été ajouté`, count);
        }
    }

}
