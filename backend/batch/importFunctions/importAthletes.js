const axios = require('axios');

exports.importAthlete = async (athletes, athletesPictures) => {
    let count = 0;

    for (const athlete of athletes) {
        const country = await axios.get(`http://localhost:8245/country/code/${athlete.country_code}`);
        const athletePicture = athletesPictures.find(athletePic => athletePic.url === athlete.athlete_url);
        console.log(country.data.data.id);
        try {
            if (athlete.athlete_full_name !== "" && athlete.athlete_full_name !== null) {
                const athleteSplitName = splitFullname(athlete.athlete_full_name);
                await axios.put("http://localhost:8245/athlete",
                    {
                        lastname: athleteSplitName.lastname,
                        firstname: athleteSplitName.firstname,
                        age: null,
                        height: null,
                        weight: null,
                        image: athletePicture ? athletePicture.imageUrl : null,
                        birth_year: null,
                        countryId: country.data.data.id,
                    }
                );

                count++;
                console.log(`Successfully added athlete: ${athlete.athlete_full_name}`, count);
            }
        } catch (error) {
            count++;
            console.error(`Error adding athlete: ${error.message} - ${athlete.athlete_full_name} a déjà été ajouté`, count);
        }
    }

}

function splitFullname(fullname) {
    const names = fullname.split(' ');
    return {
        lastname: names.slice(1).join(' '),
        firstname: names[0]
    };
}
