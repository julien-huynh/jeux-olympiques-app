const axios = require('axios');
const {generateSlug} = require("../../_utils/associationUtils");

exports.importSports = async (rows) => {
    let count = 0;

    for (const row of rows) {
        try {
            if (row.discipline_title !== "" && row.discipline_title !== null) {
                await axios.put("http://localhost:8245/sport",
                    {
                        name: row.discipline_title,
                        slug: generateSlug(row.discipline_title),
                        image: null
                    }
                );

                count++;
                console.log(`Successfully added sport: ${row.discipline_title}`, count);
            }
        } catch (error) {
            count++;
            console.error(`Error adding sport: ${error.message} - ${row.discipline_title} a déjà été ajouté`, count);
        }
    }

}
