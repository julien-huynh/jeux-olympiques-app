const axios = require('axios');

exports.fetchAndStoreCountries = async () => {
    const response = await axios.get('https://restcountries.com/v3.1/all');
    const API_URL = 'http://localhost:8245/country';
    const countries = response.data;

    try {
        for (const country of countries) {

            try {
                await axios.put(API_URL, {
                    name: country.name.common,
                    code: country.cca2,
                    longcode: country.cioc ? country.cioc : country.cca3,
                    flag: country.flags ? country.flags.png : null
                });
                console.log(`Le pays ${country.name.common} a été ajouté à la base de données.`);
            } catch (error) {
                if (error.response && error.response.data && error.response.data.code === 1) {
                    console.error(`Le pays ${country.name.common} existe déjà dans la base de données.`);
                } else {
                    console.error('Erreur lors de l\'insertion des données:', error);
                }
            }

        }

        console.log('Les pays ont été ajoutés à la base de données avec succès.');
    } catch (error) {
        console.error('Erreur lors de la récupération ou de l\'insertion des données:', error);
    }

    try {
        await axios.put(API_URL, {
            name: "ROC",
            code: "ROC",
            longcode: "ROC",
            flag: "https://gstatic.olympics.com/s1/t_s_pog_flag/f_auto/static/flag/4x3/roc"
        });
        console.log('ROC a été ajouté à la base de données avec succès.');

    } catch (error) {
        console.error('Erreur lors de l\'insertion de ROC:', error);
    }

}
