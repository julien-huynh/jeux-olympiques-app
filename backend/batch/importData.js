const fs = require('fs');
const csv = require('csv-parser');
const path = require('path');
const {importSports} = require("./importFunctions/importSports");
const {importAthlete} = require("./importFunctions/importAthletes");
const {importEventTypes} = require("./importFunctions/importEventTypes");
const {createEvents} = require("./importFunctions/createEvents");
const {fetchAndStoreCountries} = require("./importFunctions/importCountries");
const {createResults} = require("./importFunctions/createResults");
const athletesPictures = JSON.parse(fs.readFileSync('../resources/athletesPicturesTokyo2020.json', 'utf-8'));

const csvFilePath = path.resolve(__dirname, '../resources/olympic_medals.csv');
const gameEdition = 'tokyo-2020';

// Lancer la migration des seeders avant le script !!!
async function importData(filePath) {
    const data = [];

    fs.createReadStream(filePath)
        .pipe(csv())
        .on('data', (row) => {
            if (row.slug_game === gameEdition)
                data.push(row);
        })
        .on('end', async () => {
            await fetchAndStoreCountries();
            await importAthlete(data, athletesPictures);
            await importSports(data);
            await importEventTypes(data);
            await createEvents(data);
            await createResults(data);
        });
}

importData(csvFilePath);
