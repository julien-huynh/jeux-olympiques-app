const { RequestError } = require('../error/customError');

exports.addAssociation = async (paramId, Model, associationFunction, errorMessage, instanceToAssociate) => {
  try {
    if (paramId) {
      const instance = await Model.findByPk(paramId);
      if (!instance) {
        throw new RequestError(errorMessage);
      }
      // Ensure that the associationFunction exists and is a function in the instance
      if (typeof instance[associationFunction] !== 'function') {
        throw new RequestError(`Association function "${associationFunction}" not found in the model.`);
      }
      await instance[associationFunction](instanceToAssociate);
    }
  } catch (error) {
    return { success: false, message: errorMessage };
  }
  return { success: true, message: 'Association added successfully.' };
};

exports.generateSlug = function (name) {
  return name.toLowerCase().replace(/\s+/g, '-');
}
