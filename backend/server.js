/* Import des modules */
const express = require("express");
const cors = require("cors");
const path = require('path');

// const check = require("./jsonwebtoken/check");
const errorHandler = require("./error/errorHandler");

/* Import de la connexion à la base de données */
let DB = require("./db.config");

/* Initialisation de l'API */
const app = express();

// Configuration CORS pour permettre l'accès depuis le frontend
const corsOptions = {
  origin: "http://frontend.com",
  optionsSuccessStatus: 200 // Certains navigateurs anciens réclament cette option
};

app.use(cors(corsOptions));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

/* Servir les fichiers statiques */ 
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));

/* Import des modules de routage */
const authRouter = require("./routes/auth");
const userRouter = require("./routes/users");
const countryRouter = require("./routes/countries");
const athleteRouter = require("./routes/athletes");
const medalRouter = require("./routes/medals");
const hostRouter = require("./routes/hosts");
const eventTypeRouter = require("./routes/eventTypes");
const categoryRouter = require("./routes/categories");
const sportRouter = require("./routes/sports");
const eventRouter = require("./routes/events");
const resultRouter = require("./routes/results");

/* Mise en place du routage */
app.get("/", (req, res) =>
  res.send(`Bienvenue sur l'API des jeux olympiques !`)
);
app.use("/auth", authRouter);
app.use("/user", userRouter);
app.use("/country", countryRouter);
app.use("/medal", medalRouter);
app.use("/host", hostRouter);
app.use("/eventType", eventTypeRouter);
app.use("/category", categoryRouter);
app.use("/sport", sportRouter);
app.use("/athlete", athleteRouter);
app.use("/event", eventRouter);
app.use("/result", resultRouter);
app.use(errorHandler);


/* Demarrage serveur avec test DB */
DB.sequelize
  .authenticate()
  .then(() => console.log("Database connection OK"))
  .then(() => {
    app.listen(process.env.SERVER_PORT, () => {
      console.log(
        `Server running on ${process.env.SERVER_PORT}. Enjoy your stay !`
      );
    });
  })
  .catch((err) => console.log("Database Error", err));