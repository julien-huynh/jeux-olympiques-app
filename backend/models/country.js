/* Import des modules nécessaires */
const { DataTypes } = require("sequelize");

/* Définition du modèle */
module.exports = (sequelize) => {
  const Country = sequelize.define(
    "Country",
    {
      id: {
        type: DataTypes.INTEGER(),
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      slug: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      code: {
        type: DataTypes.STRING(3),
        allowNull: true,
      },
      longcode: {
        type: DataTypes.STRING(3),
        unique: true,
        allowNull: true,
      },
      flag: {
        type: DataTypes.STRING(200),
        allowNull: true,
      },
    },
    { paranoid: true } // softDelete
  );

  return Country;
};
