/* Import des modules nécessaires */
const { DataTypes } = require("sequelize");

/* Définition du modèle */
module.exports = (sequelize) => {
  const Medal = sequelize.define(
    "Medal",
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      color: {
        type: DataTypes.STRING(20),
        allowNull: false,
        unique: true,
      },
    },
    { paranoid: true } // softDelete
  );

  return Medal;
};
