/* Import des modules nécessaires */
const { DataTypes } = require("sequelize");

/* Définition du modèle */
module.exports = (sequelize) => {
  const Event = sequelize.define(
    "Event",
    {
      id: {
        type: DataTypes.INTEGER(),
        primaryKey: true,
        autoIncrement: true,
      },
      date: {
        type: DataTypes.DATE(),
        allowNull: true,
      },
    },
    { paranoid: true } // softDelete
  );

  return Event;
};
