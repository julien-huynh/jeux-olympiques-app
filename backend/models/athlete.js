/* Import des modules nécessaires */
const { DataTypes } = require("sequelize");

/* Définition du modèle */
module.exports = (sequelize) => {
  const Athlete = sequelize.define(
    "Athlete",
    {
      id: {
        type: DataTypes.INTEGER(),
        primaryKey: true,
        autoIncrement: true,
      },
      lastname: {
        type: DataTypes.STRING(30),
        allowNull: false,
      },
      slug: {
        type: DataTypes.STRING(100),
        allowNull: false,
        unique: true,
      },
      firstname: {
        type: DataTypes.STRING(30),
        allowNull: false,
      },
      birth_year: {
        type: DataTypes.INTEGER(),
        allowNull: true,
      },
      height: {
        type: DataTypes.FLOAT(),
        allowNull: true,
      },
      weight: {
        type: DataTypes.INTEGER(),
        allowNull: true,
      },
      image: {
        type: DataTypes.STRING(200),
        allowNull: true,
      },
    },
    { paranoid: true } // softDelete
  );

  return Athlete;
};
