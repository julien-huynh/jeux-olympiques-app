class MainError extends Error {
  constructor(errorMessage, errorType = "") {
    super();

    this.name = this.constructor.name;
    this.message = errorMessage;

    switch (this.constructor.name) {
      case "AuthenticationError":
        if (errorType == 0) {
          this.statusCode = 400;
        } else if (errorType == 1) {
          this.statusCode = 404;
        } else {
          this.statusCode = 401;
        }
        break;
      case "UserError":
        if (errorType == 0) {
          this.statusCode = 404;
        } else {
          this.statusCode = 409;
        }
        break;
      case "AthleteError":
        if (errorType == 0) {
          this.statusCode = 404;
        } else {
          this.statusCode = 409;
        }
        break;
      case "HostError":
        if (errorType == 0) {
          this.statusCode = 404;
        } else {
          this.statusCode = 409;
        }
        break;
      case "EventError":
        if (errorType == 0) {
          this.statusCode = 404;
        } else {
          this.statusCode = 409;
        }
        break;
      case "EventTypeError":
        if (errorType == 0) {
          this.statusCode = 404;
        } else {
          this.statusCode = 409;
        }
        break;
      case "ResultError":
        if (errorType == 0) {
          this.statusCode = 404;
        } else {
          this.statusCode = 409;
        }
        break;
      case "CountryError":
        if (errorType == 0) {
          this.statusCode = 404;
        } else {
          this.statusCode = 409;
        }
        break;
      case "MedalError":
        if (errorType == 0) {
          this.statusCode = 404;
        } else {
          this.statusCode = 409;
        }
        break;
      case "SportError":
        if (errorType == 0) {
          this.statusCode = 404;
        } else {
          this.statusCode = 409;
        }
        break;
      case "CategoryError":
        if (errorType == 0) {
          this.statusCode = 404;
        } else {
          this.statusCode = 409;
        }
        break;
      case "RequestError":
        this.statusCode = 400;
        break;
      default:
        console.log("Pas de systeme de gestion d'exception pour cette erreur");
    }
  }
}

class AuthenticationError extends MainError {}

class UserError extends MainError {}

class RequestError extends MainError {}

class AthleteError extends MainError {}

class MedalError extends MainError {}

class SportError extends MainError {}

class HostError extends MainError {}

class ResultError extends MainError {}

class EventError extends MainError {}

class EventTypeError extends MainError {}

class CountryError extends MainError {}

class CategoryError extends MainError {}

module.exports = {
  MainError,
  AuthenticationError,
  UserError,
  RequestError,
  AthleteError,
  CountryError,
  MedalError,
  SportError,
  HostError,
  EventError,
  EventTypeError,
  CategoryError,
  ResultError,
};
