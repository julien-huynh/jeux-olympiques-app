"use strict";
const bcrypt = require("bcryptjs");
/** @type {import('sequelize-cli').Migration} */
module.exports = {
	async up(queryInterface, Sequelize) {
		let hashadmin = await bcrypt.hash(
			"testadmin",
			parseInt(process.env.BCRYPT_SALT_ROUND)
		);
		let hashuser = await bcrypt.hash(
			"testuser",
			parseInt(process.env.BCRYPT_SALT_ROUND)
		);

		// Hotes
		await queryInterface.bulkInsert(
			"Hosts",
			[
				{
					name: "Tokyo 2020",
					slug: "tokyo-2020",
					createdAt: new Date(),
					updatedAt: new Date(),
					deletedAt: null
				},
				{
					name: "Paris 2024",
					slug: "paris-2024",
					createdAt: new Date(),
					updatedAt: new Date(),
					deletedAt: null
				},
				{
					name: "Los Angeles 2028",
					slug: "los-angeles-2028",
					createdAt: new Date(),
					updatedAt: new Date(),
					deletedAt: null
				},
			]
			,
			{}
		);

		// Sports
		await queryInterface.bulkInsert(
			"Sports",
			[
				{
					name: "Basketball",
					slug: "basketball",
					createdAt: new Date(),
					updatedAt: new Date(),
					deletedAt: null,
					image: null
				},
				{
					name: "Athlétisme",
					slug: "athletisme",
					createdAt: new Date(),
					updatedAt: new Date(),
					deletedAt: null,
					image: null
				},
				{
					name: "Lancer de javelot",
					slug: "lancer-de-javelot",
					createdAt: new Date(),
					updatedAt: new Date(),
					deletedAt: null,
					image: null
				},
			]
			,
			{}
		);

		// Categories
		await queryInterface.bulkInsert(
			"Categories",
			[
				{
					name: "Men",
					createdAt: new Date(),
					updatedAt: new Date(),
					deletedAt: null,
				},
				{
					name: "Mixed",
					createdAt: new Date(),
					updatedAt: new Date(),
					deletedAt: null,
				},
				{
					name: "Women",
					createdAt: new Date(),
					updatedAt: new Date(),
					deletedAt: null,
				},
			]
			,
			{}
		);

		// EventTypes
		await queryInterface.bulkInsert(
			"Event_types",
			[
				{
					name: "Basketball",
					slug: "basketball",
					createdAt: new Date(),
					updatedAt: new Date(),
					deletedAt: null,
				},
				{
					name: "100 metres",
					slug: "100-metres",
					createdAt: new Date(),
					updatedAt: new Date(),
					deletedAt: null,
				},
				{
					name: "200 metres",
					slug: "200-metres",
					createdAt: new Date(),
					updatedAt: new Date(),
					deletedAt: null,
				},
			]
			,
			{}
		);

		// Médailles
		await queryInterface.bulkInsert(
			"Medals",
			[
				{
					color: "GOLD",
					createdAt: new Date(),
					updatedAt: new Date(),
					deletedAt: null
				},
				{
					color: "SILVER",
					createdAt: new Date(),
					updatedAt: new Date(),
					deletedAt: null
				},
				{
					color: "BRONZE",
					createdAt: new Date(),
					updatedAt: new Date(),
					deletedAt: null
				},
			]
			,{}
		);

		// Utilisateurs
		await queryInterface.bulkInsert(
			"Users",
			[
				{
					name: "testadminNAME",
					email: "test@admin.test",
					slug: "testadminNAME",
					firstname: "testadminFIRSTNAME",
					username: "testadminUSERNAME",
					password: hashadmin,
					roles: "ROLE_ADMIN",
					createdAt: new Date(),
					updatedAt: new Date(),
					deletedAt: null

				},
				{
					name: "testuserNAME",
					email: "test@user.test",
					slug: "testuserNAME",
					firstname: "testuserFIRSTNAME",
					username: "testuserUSERNAME",
					password: hashuser,
					roles: "ROLE_USER",
					createdAt: new Date(),
					updatedAt: new Date(),
					deletedAt: null
				},
			],
			{}
		);
	},

	async down(queryInterface, Sequelize) {
		// await queryInterface.bulkDelete({
		// 	model: "Users",
		// 	where: { name: { [Op.or]: ["testadminNAME", "testuserNAME"] } },
		// });
		await queryInterface.bulkDelete("Users", { name: ["testuserNAME","testadminNAME"] } );
		await queryInterface.bulkDelete("Medals", { color: ["GOLD","SILVER","BRONZE"] } );
		await queryInterface.bulkDelete("Categories", { name: ["Hommes","Femmes","Paralympique Hommes"] } );
		await queryInterface.bulkDelete("Event_types", { name: ["Basketball","100 metres","200 metres"] } );
		await queryInterface.bulkDelete("Sports", { name: ["Basketball","Athlétisme","Lancer de javelot"] } );
		await queryInterface.bulkDelete("Hosts", { name: ["Tokyo 2020","Paris 2024","Los Angeles 2028"] } );
	},
};
